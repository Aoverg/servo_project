#include <Servo.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);

#define buzzerPin 7
#define btn_left A0
#define btn_right A1
#define btn_down A2
#define btn_up A3
#define fire_btn 5

Servo rotate;
Servo tilt;
float pos_tilt = 0.0; 
float pos_rotate = 0.0;
float step = 1.0;

int bullets = 20;

void setup()
{
  pinMode(btn_left,   INPUT_PULLUP);
  pinMode(btn_right,  INPUT_PULLUP);
  pinMode(btn_down,   INPUT_PULLUP);
  pinMode(btn_up,     INPUT_PULLUP);
  pinMode(fire_btn,   INPUT_PULLUP);
  
  pinMode(buzzerPin, OUTPUT);

  rotate.attach(2);
  rotate.write(pos_rotate);
  tilt.attach(3);
  tilt.write(pos_tilt);

  lcd.init();                      // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Bullets left:");
  lcd.setCursor(14,0);
  lcd.print(bullets);
}

void moveServo(Servo &arm, float &pos, int mode){
  switch(mode){
    case 0: //move servo closer 0. degree
      if (pos>0){
        arm.write(pos);
        pos-=step;
        delay(5);
    }
      break;
    case 1: //move servo closer 180. degree
      if (pos<180) {
        arm.write(pos);
        pos+=step;
        delay(5);
      }
      break;
    default:
      break;
  }   
}

void shotFired(int &bullets){
  if(bullets > 0){
    bullets -= 1;

    lcd.setCursor(14,0);
    lcd.print("   ");
    lcd.setCursor(14,0);
    lcd.print(bullets);

    tone(buzzerPin, 70, 200);
  }
}

void loop()
{  

  if (!digitalRead(btn_left)){
    moveServo(rotate, pos_rotate, 0);
  }
  if (!digitalRead(btn_right)){
    moveServo(rotate, pos_rotate, 1);
  }
  if (!digitalRead(btn_up)){
    moveServo(tilt, pos_tilt, 1);
  }
  if (!digitalRead(btn_down)){
    moveServo(tilt, pos_tilt, 0);
  }
  if (digitalRead(fire_btn) == LOW){
    shotFired(bullets);
    delay(1000);
  }

}
